# mouseRightMenu-layui扩展模块


#### 文档地址:http://doc.enianteam.com/layui_module/48.html
#### 演示地址:http://doc.enianteam.com/demo/mouse_right_menu.html

#### 介绍
左右键菜单，弹出菜单 mouseRightMenu.js 基于layui开发的扩展模块。
支持跟随鼠标弹出，更多内容请查看上面文档

#### bug/建议
微信：95302870
邮箱：95302870@qq.com